import Vue from 'vue'
import App from './App'
import store from "./store/index.js"
import '@nwm/mapp-app-core/api/product-index.js'

Vue.config.productionTip = false;

App.mpType = 'app'

const app = new Vue({
  ...App,
  store
})
app.$mount()
//！！此注释勿删，自动收集注册全局组件
import nwmnwmnavigationbar from "@nwm/mapp-app-core/components/modules/nwm-navigation-bar.vue"
Vue.component("nwm-nwm-navigation-bar", nwmnwmnavigationbar)
import nwmnwmimage from "@nwm/mapp-app-core/components/modules/nwm-image.vue"
Vue.component("nwm-nwm-image", nwmnwmimage)
import nwmnwmmulimage from "@nwm/mapp-app-core/components/modules/nwm-mul-image.vue"
Vue.component("nwm-nwm-mul-image", nwmnwmmulimage)
import nwmnwmsearch from "@nwm/mapp-app-core/components/modules/nwm-search.vue"
Vue.component("nwm-nwm-search", nwmnwmsearch)
import nwmnwmbtngroup from "@nwm/mapp-app-core/components/modules/nwm-btn-group.vue"
Vue.component("nwm-nwm-btn-group", nwmnwmbtngroup)
import nwmnwmlist from "@nwm/mapp-app-core/components/modules/nwm-list.vue"
Vue.component("nwm-nwm-list", nwmnwmlist)
import nwmnwmtile from "@nwm/mapp-app-core/components/modules/nwm-tile.vue"
Vue.component("nwm-nwm-tile", nwmnwmtile)
import nwmnwmlogout from "@nwm/mapp-app-core/components/modules/nwm-logout.vue"
Vue.component("nwm-nwm-logout", nwmnwmlogout)
import nwmnwmpersoninfo from "@nwm/mapp-app-core/components/modules/nwm-person-info.vue"
Vue.component("nwm-nwm-person-info", nwmnwmpersoninfo)
import nwmnwmaboutinfo from "@nwm/mapp-app-core/components/modules/nwm-about-info.vue"
Vue.component("nwm-nwm-about-info", nwmnwmaboutinfo)
import nwmnwmbuttonfloat from "@nwm/mapp-app-core/components/modules/nwm-button-float.vue"
Vue.component("nwm-nwm-button-float", nwmnwmbuttonfloat)
import nwmpagelogotitlelogin from "@nwm/mapp-app-product-core/pages/login/page-logo-title-login"
Vue.component("nwm-page-logo-title-login", nwmpagelogotitlelogin)
import nwmpagetitlelogin from "@nwm/mapp-app-product-core/pages/login/page-title-login"
Vue.component("nwm-page-title-login", nwmpagetitlelogin)
import nwmpagedesigncontainer from "@nwm/mapp-app-product-core/pages/page-design-container"
Vue.component("nwm-page-design-container", nwmpagedesigncontainer)
import nwmpagewebview from "@nwm/mapp-app-product-core/pages/page-webview"
Vue.component("nwm-page-webview", nwmpagewebview)
import nwmhomepage from "@nwm/mapp-app-product-core/pages/home-page"
Vue.component("nwm-home-page", nwmhomepage)
import extendextendmodule from "@extend/extend-test-core/components/modules/extend-module.vue"
Vue.component("extend-extend-module", extendextendmodule)
import extendextendproppanel from "@extend/extend-test-core/components/prop-panels/extend-prop-panel.vue"
Vue.component("extend-extend-prop-panel", extendextendproppanel)
import extendrpreimbursement from "@extend/extend-test-product-core/pages/rp-reimbursement"
Vue.component("extend-rp-reimbursement", extendrpreimbursement)
import extendextendleavelist from "@extend/extend-test-product-core/pages/extend-leave-list"
Vue.component("extend-extend-leave-list", extendextendleavelist)
import extendextendleavedetail from "@extend/extend-test-product-core/pages/extend-leave-detail"
Vue.component("extend-extend-leave-detail", extendextendleavedetail)
import extendextendlogin from "@extend/extend-test-product-core/pages/extend-login"
Vue.component("extend-extend-login", extendextendlogin)
//！！此注释勿删，自动收集注册全局组件
