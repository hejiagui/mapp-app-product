export default{

    setPhoneWidth(state, width){
        state.phoneWidth = width;
    },

    setPhoneHeight(state, height){
        state.phoneHeight = height;
    },
	setTabBarHeight(state, height){
	    state.tabBarHeight = height;
	},
}