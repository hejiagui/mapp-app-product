export default{

    setPhoneWidth(context, param){
        context.commit("setPhoneWidth", param);
    },

    setPhoneHeight(context, param){
        context.commit("setPhoneHeight", param);
    },
	setTabBarHeight(context, param){
	    context.commit("setTabBarHeight", param);
	},
}